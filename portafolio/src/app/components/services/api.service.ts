import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http : HttpClient) { }
  
  postClientes(data : any){
    // data.id = 1
    // localStorage.setItem('listaClientes', JSON.stringify([data]));
    // console.log(data);
    
    // para hacer deploy:
    // https://duncanhunter.gitbook.io/enterprise-angular-applications-with-ngrx-and-nx/introduction/4-add-json-server
    return this.http.post<any>('http://localhost:3000/listaClientes/',data);
  }
  getClientes(){
    // let storage = localStorage.getItem('listaClientes');
    // if (storage) {
    //   const data = JSON.parse(storage);
    //   console.log(data);
    // }
    return this.http.get<any>('http://localhost:3000/listaClientes/');
  }
  putClientes(data: any, id : number){
    return this.http.put<any>('http://localhost:3000/listaClientes/'+id, data);
  }

  deleteClientes(id: number){
    return this.http.delete<any>('http://localhost:3000/listaClientes/'+id);
  }
}
