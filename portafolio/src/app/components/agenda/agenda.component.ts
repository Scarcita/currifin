import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog} from '@angular/material/dialog';
import { DialogoComponent } from '../dialogo/dialogo.component';
import { ApiService } from '../services/api.service';
import { MatPaginator} from '@angular/material/paginator';
import { MatSort} from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-agenda',
  templateUrl: './agenda.component.html',
  styleUrls: ['./agenda.component.css']
})
export class AgendaComponent implements OnInit {

  displayedColumns: string[] = ['Nombre', 'Apellidos', 'Email', 'Celular', 'Hora', 'Servicio', 'Fecha',  'Descripcion', 'Accion'];
  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor( private dialog : MatDialog,
              private api : ApiService)
              { }

  ngOnInit(): void {
    this.getAllClientes();
  }

  openDialog() {
    this.dialog.open(DialogoComponent, {
     width: '60%', 
    }).afterClosed().subscribe(val=> {
      if(val === 'save'){
        this.getAllClientes
      }
    })
  }
  getAllClientes() {
    this.api.getClientes()
    .subscribe({
      next: (res) => {
        this.dataSource = new MatTableDataSource(res);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort  
      },
      error: (err) => {
        alert("Error de busqueda!")
      }
    });
  }
  
  editCita(row : any ){
    this.dialog.open(DialogoComponent,{
      width: '60%',
      data: row
    }).afterClosed().subscribe(val => {
      if(val==='update'){
        this.getAllClientes();
      }
    })
  }
  deleteClientes(id: number){


    this.api.deleteClientes(id)
    .subscribe({
      next: (res)=> {
        alert("Su cita sera aleminada ¿Esta seguro(a)?");
        this.getAllClientes();
    
      },
      error:()=> {
        alert("error en la eliminacion")
      }
    })

  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
